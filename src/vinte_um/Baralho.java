package vinte_um;

import java.util.ArrayList;
import java.util.Collections;


public class Baralho {
	
	ArrayList<Carta> baralho = new ArrayList<Carta>();
	
	Baralho(){
		
		String naipes[] = {"Ouros", "Espadas", "Copas", "Paus"};
		String cartas[] = {"Ás", "Dois", "Três", "Quatro", "Cinco", "Seis", "Sete", 
							"Oito", "Nove", "Dez", "Valete", "Dama", "Rei"};
		int[] valores = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10};
		
		int indiceBaralho = 0;
		
		for (int i = 0; i < cartas.length; i++) {
			for (int j = 0; j < naipes.length; j++) {
				Carta carta = new Carta();
				carta.naipe = naipes[j];
				carta.numero = cartas[i];
				carta.valor = valores[i];
				
				baralho.add(carta);
				indiceBaralho++;
			}
			
		}
	}
	
	public Carta removerCarta() {
		return baralho.remove(baralho.size() - 1);
	}
	
	public void embaralhar() {
		Collections.shuffle(baralho);
	}

}

package vinte_um;

public class Carta {
	
	String naipe;
	String numero;
	int valor;
	
	public String imprimeCarta() {
		return String.format("%s de %s", numero, naipe);
	}

}

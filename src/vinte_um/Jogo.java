package vinte_um;

import java.util.ArrayList;
import java.util.Scanner;

public class Jogo {
	
	public static void main (String args[]) {
		
		Scanner scanner = new Scanner(System.in);
		
		ArrayList<Jogador> listaJogadores = new ArrayList<Jogador>();
		int quantidadeJogadores = 0;
		System.out.println("Inicio do jogo");
		
		do {
			System.out.println("Quantos jogadores participarao ? (minimo de 2 jogadores) ");
			 quantidadeJogadores = scanner.nextInt();
		} while (quantidadeJogadores < 2);
		
		Baralho baralho = new Baralho();
		
		baralho.embaralhar();
		
		for (int i = 1; i < quantidadeJogadores + 1; i++) {
			System.out.println("Digite o nome do jogador " + i + ": ");
			String nomeJogador = scanner.next();
			Jogador jogador = new Jogador(nomeJogador);
			listaJogadores.add(jogador);
			jogador.adicionarCarta(baralho.removerCarta());
			jogador.adicionarCarta(baralho.removerCarta());
		}
		
		// imprime cartas de cada jogador
		for(Jogador jogador : listaJogadores) {
			imprimeMaoJogador(jogador);
		}
		
		ArrayList<Integer> listaExcluidosIndice = new ArrayList<Integer>();
		
		do {
			
			for(Jogador jogador : listaJogadores ) {
				if(jogador.isQuerContinuarJogando()) {
					System.out.println("Jogador " + jogador.nome + " deseja comprar mais cartas ? 0-Sim e 1-Nao");
					int resposta = scanner.nextInt();
					
					if(resposta == 0) {
						jogador.adicionarCarta(baralho.removerCarta());
					} else {
						jogador.setQuerContinuarJogando(false);
					}
				}
			}
			
			for(Jogador jogador : listaJogadores ) {
				System.out.println("--------------");
				System.out.println("Jogador " + jogador.nome + " possui as seguintes cartas: ");
				for(Carta carta : jogador.mao) {
					System.out.println(carta.imprimeCarta());
				}
				System.out.println("total = " + jogador.getPontos());
				if(jogador.getPontos() > 21) {
					System.out.println("Jogador " + jogador.nome + " esta fora pois estourou com " + jogador.getPontos() + " pontos!");
					listaExcluidosIndice.add(listaJogadores.indexOf(jogador));
				}
			}
			
			// Excluir os jogadores que estouraram
			if (listaExcluidosIndice.size() > 0) {
				for (int i = listaExcluidosIndice.size() - 1; i >= 0; i--) {
				listaJogadores.remove(listaJogadores.get(listaExcluidosIndice.get(i)));
				}
			}
			
			listaExcluidosIndice.clear();
			
		} while (verificarFimJogo(listaJogadores));
		

		
		verificarGanhador(listaJogadores);
	}
	
	public static boolean verificarFimJogo(ArrayList<Jogador> listaJogadores) {
		
		if(listaJogadores.size() <= 1) {
			return false;
		}
		
		for(Jogador jogador : listaJogadores) {
			if(jogador.isQuerContinuarJogando())
				return true;
		}
		
		return false;
		
	}
	
	public static void verificarGanhador(ArrayList<Jogador> listaJogadores) {
		
		if (listaJogadores.size() == 0) {
			System.out.println("Nao houve vencedores.");
			return;
		} else if (listaJogadores.size() == 1) {
			System.out.println("Jogador " + listaJogadores.get(0).nome + " venceu!");
			return;
		}
		
		
		// Procurar a maior pontuacao
		int maiorPontuacao = 0;
		
		for(Jogador jogador : listaJogadores) {
			if(jogador.getPontos() > maiorPontuacao) {
				maiorPontuacao = jogador.getPontos();
			}
		}
		
		// Excluir jogadores que nao possuem a maior pontuacao
		for(Jogador jogador : listaJogadores) {
			if(jogador.getPontos() != maiorPontuacao) {
				listaJogadores.remove(jogador);
			}
		}
		
		if(listaJogadores.size() == 1) {
			System.out.println("Jogador " + listaJogadores.get(0).nome + " venceu!");
		} else {
			System.out.println("Houve empate entre os jogadores: ");
			for(Jogador jogador : listaJogadores) {
				System.out.println(jogador.nome);
			}
		}
		
	}
	
	public static void imprimeMaoJogador(Jogador jogador) {
		System.out.println("Jogador " + jogador.nome + " possui:");
		
		for (Carta carta : jogador.mao) {
			System.out.println(carta.imprimeCarta() + " " + " = " + carta.valor);
		}
		
		System.out.println("---------------");
		System.out.println("total: " + jogador.getPontos() + " pontos");
		
		System.out.println("");
	}
}
